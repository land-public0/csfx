cd /usr/src
rm -fv csf.tgz
wget https://download.configserver.com/csf.tgz
tar -xzf csf.tgz
cd csf
sh install.sh

cd /etc/csf
sed -i 's/TESTING = "1"/TESTING = "0"/g' csf.conf
sed -i 's/TCP_IN = "20,21,22,25,53,80,110,143,443,465,587,993,995"/TCP_IN = "20,21,22,53,1881,8080"/g' csf.conf
sed -i 's/TCP_OUT = "20,21,22,25,53,80,110,113,443,587,993,995"/TCP_OUT = "20,21,22,53,80,443,8080"/g' csf.conf
sed -i 's/IPV6 = "1"/IPV6 = "0"/g' csf.conf
sed -i 's/TCP6_IN = "20,21,22,25,53,80,110,143,443,465,587,993,995"/TCP6_IN = "20,21,22"/g' csf.conf

sed -i 's/SYNFLOOD_RATE = "100"/SYNFLOOD_RATE = "30"/g' csf.conf
sed -i 's/SYNFLOOD = "0"/SYNFLOOD = "1"/g' csf.conf
sed -i 's/UDPFLOOD = "0"/UDPFLOOD = "1"/g' csf.conf

csf -e
csf -r

